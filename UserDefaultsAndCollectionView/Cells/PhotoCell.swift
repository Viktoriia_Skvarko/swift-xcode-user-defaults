//  PhotoCell.swift
//  UserDefaultsAndCollectionView
//  Created by Viktoriia Skvarko


import UIKit

class PhotoCell: UICollectionViewCell {
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var photo: Photo?
    
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var chekBoxButtonOutlet: UIButton!
    
    
    @IBAction func checkBox(_ sender: UIButton) {
        chekBoxButtonOutlet.isSelected = !chekBoxButtonOutlet.isSelected
        UserDefaults.standard.setValue(chekBoxButtonOutlet.isSelected, forKey: photo!.nameCell)
    }
    
}





