//  ViewController.swift
//  UserDefaultsAndCollectionView
//  Created by Viktoriia Skvarko


import UIKit

class CollectionVC: UIViewController {
    
    @IBOutlet weak var collOutlet: UICollectionView!
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let itemsPerRow = 3
    let margin: CGFloat = 5
    
}


extension CollectionVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PhotoStorage.photoCats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = PhotoStorage.itemCatDog(forIndex: indexPath.item)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.imageCell.image = UIImage(named: item.imageName)
        cell.photo = item
        cell.chekBoxButtonOutlet.isSelected = UserDefaults.standard.bool(forKey: item.nameCell)
        
        return cell
        
    }
    
}


extension CollectionVC: UICollectionViewDelegate {
    
}


extension CollectionVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (UIScreen.main.bounds.width - CGFloat(itemsPerRow + 1) * margin) / CGFloat(itemsPerRow)
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 15, left: 0, bottom: margin, right: 0)
    }
    
}


