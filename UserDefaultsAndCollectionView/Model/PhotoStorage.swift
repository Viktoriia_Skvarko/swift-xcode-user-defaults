//  PhotoStorage.swift
//  UserDefaultsAndCollectionView
//  Created by Viktoriia Skvarko


import Foundation
import UIKit

struct PhotoStorage {
    
    static let photoCats: Array<Photo> = [
        Photo(imageName: "cat1", nameCell: "0001"),
        Photo(imageName: "cat2", nameCell: "0002"),
        Photo(imageName: "cat3", nameCell: "0003"),
        Photo(imageName: "dog1", nameCell: "0004"),
        Photo(imageName: "dog2", nameCell: "0005"),
        Photo(imageName: "dog3", nameCell: "0006"),
        Photo(imageName: "cat1", nameCell: "0007"),
        Photo(imageName: "cat2", nameCell: "0008"),
        Photo(imageName: "cat3", nameCell: "0009"),
        Photo(imageName: "dog1", nameCell: "0010"),
        Photo(imageName: "dog2", nameCell: "0011"),
        Photo(imageName: "dog3", nameCell: "0012"),
        Photo(imageName: "cat1", nameCell: "0013"),
        Photo(imageName: "cat2", nameCell: "0014"),
        Photo(imageName: "cat3", nameCell: "0015"),
        Photo(imageName: "dog1", nameCell: "0016"),
        Photo(imageName: "dog2", nameCell: "0017"),
        Photo(imageName: "dog3", nameCell: "0018"),
        Photo(imageName: "cat1", nameCell: "0019"),
        Photo(imageName: "cat2", nameCell: "0020"),
        Photo(imageName: "cat3", nameCell: "0021"),
        Photo(imageName: "dog1", nameCell: "0022"),
        Photo(imageName: "dog2", nameCell: "0023"),
        Photo(imageName: "dog3", nameCell: "0024"),
        Photo(imageName: "cat1", nameCell: "0025"),
        Photo(imageName: "cat2", nameCell: "0026"),
        Photo(imageName: "cat3", nameCell: "0027"),
        Photo(imageName: "dog1", nameCell: "0028"),
        Photo(imageName: "dog2", nameCell: "0029"),
        Photo(imageName: "dog3", nameCell: "0030"),
        Photo(imageName: "cat1", nameCell: "0031"),
        Photo(imageName: "cat2", nameCell: "0032"),
        Photo(imageName: "cat3", nameCell: "0033"),
        Photo(imageName: "dog1", nameCell: "0034"),
        Photo(imageName: "dog2", nameCell: "0035"),
        Photo(imageName: "dog3", nameCell: "0036")
        
    ]
    
    static func itemCatDog(forIndex index: Int) -> Photo {
        return photoCats[index % photoCats.count]
    }
    
}
