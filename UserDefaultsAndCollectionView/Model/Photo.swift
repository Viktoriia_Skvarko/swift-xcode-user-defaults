//  Photo.swift
//  UserDefaultsAndCollectionView
//  Created by Viktoriia Skvarko


import Foundation
import UIKit

struct Photo {
    var imageName: String
    var nameCell: String
}
